import { EscRoomPage } from './app.po';

describe('esc-room App', () => {
  let page: EscRoomPage;

  beforeEach(() => {
    page = new EscRoomPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
