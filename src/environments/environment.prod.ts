// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBm3g8wwwCPBLkMHxpKzc2oUqcclXUIvqY",
    authDomain: "escaperooms-7dfd2.firebaseapp.com",
    databaseURL: "https://escaperooms-7dfd2.firebaseio.com",
    projectId: "escaperooms-7dfd2",
    storageBucket: "",
    messagingSenderId: "897447991601",
    apiUrl: "https://us-central1-escaperooms-7dfd2.cloudfunctions.net"
  }
};
