// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDBdVUwv-RmvIaAICgSHhWWS7GqtZlGJuc",
    authDomain: "escape-develop.firebaseapp.com",
    databaseURL: "https://escape-develop.firebaseio.com",
    projectId: "escape-develop",
    storageBucket: "",
    messagingSenderId: "258730487467",
    apiUrl: "https://us-central1-escape-develop.cloudfunctions.net"
  }
};
