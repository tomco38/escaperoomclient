import {Component, ViewChild} from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { PanlesManager } from './Panels';
import { DataService } from './dataService';
import { Room } from './models/room';
import { SubRoom } from './models/subRoom';
import {Order} from "./models/order";
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { AngularFirestore } from 'angularfire2/firestore';
import {CustomerDetailsFormComponent} from "./components/customer-details-form/customer-details-form.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends PanlesManager {

  @ViewChild("form") form: CustomerDetailsFormComponent;
  orderCompletedSuccessfully = false;
  appointmentInvalid = false;
  room: Room;
  lockingDate = false;

  order: Order;
  PanelsManager: PanlesManager;
  isTransDetailsReceived = false;
  dataService: DataService;
  roomId: string = 'kp6ik7o8p5ypw1ew';
  selectedSubRoom: SubRoom;

  sleepPromise(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async sleep(ms) {
    await this.sleepPromise(ms);
  }

  constructor(private http: Http, public modal: Modal, private afs: AngularFirestore) {
    super(5);
    this.dataService = new DataService(this.http, this.afs);
    this.order = new Order();
    this.PanelsManager = new PanlesManager(4);
  }

  nextAfter(func, event){
    func.call(this, event);
    this.Next();
  }

  onAfterChooseRoom(room) {
    this.order.room = room.room;
    this.order.roomDetails = room.selectedRoom;
    this.selectedSubRoom = room.selectedRoom;
  }

  removeDateLock(){
    localStorage.removeItem("dateIsLocked");
  }

  onAfterDateChoose(date){
    var orderDate = this.order.date? this.order.date._Date : this.order.date;
    var orderTime= this.order.hour? this.order.hour.Time : this.order.hour;
    if (( orderDate != date.date._date) || orderTime != date.hour.Time) {
      this.removeDateLock();
    }
    this.order.date = date.date;
    this.order.hour = date.hour;
    if(this.form){
      this.form.onAfterDateSelect();
    }
  }

  onAfterFormSubmit(data){
    this.order.customer = data.customer;
    this.order.numberOfPlayers = data.numberOfPlayers;

    return this.saveOrderDetails(this.order).subscribe((data) => {

        this.Next();
        this.form.onAfterOrderSaved();
    });

  }

  onCreditGuardCallback(data){
    this.order.transactionInfo.token = data.token;
    this.order.transactionInfo.transactionId = data.transactionId;
    window.scrollTo(0, document.body.scrollHeight);
  }

  private handleTransDetailsResult(res) {
    if (res.status === "000" /*success*/) {
      this.removeDateLock();
      this.orderCompletedSuccessfully = true;
      this.order.transactionInfo.transactionId = res.tranId;
      var vcitaClient = this.order.customer.getVcitaClientJsonFormat();
      this.dataService.createNewClientAndAppointmentInVcita(
        Object.assign(vcitaClient, this.order.getAppointmentVcita(), this.order)).subscribe((status) => {
        this.isTransDetailsReceived = true;
        if (status === "1" /*success*/) {
        }
        else this.appointmentInvalid = true;
      }, (err) => {
        this.sendMail("ויסיטה");
        this.isTransDetailsReceived = true;
        this.appointmentInvalid = true;
      });
    }
    else {
      this.sendMail("קרדיט גארד");
      this.orderCompletedSuccessfully = false;
    }

    this.sleep(4500);
  }

  sendMail(error){
    this.dataService.sendErrorToMail((this.order.getMail(error))).subscribe((s)=>{});
  }
  RandomAccessChangePage(pageTo: number) {
    // if (this.orderCompletedSuccessfully) {
    //   this.Panels[this.CurrentPanel].Show = false;
    //   this.CurrentPanel = pageTo;
    //   this.Panels[this.CurrentPanel].Show = true;
    // }
    // else if (pageTo === 4) return;
    // else if (this.CurrentPanel - 1 === pageTo) {
    //   this.Back();
    // }
    // else if (this.CurrentPanel + 1 === pageTo) {
    //   this.Next();
    // }
  }

  private saveOrderDetails(order: Order) {
    var orderDetails = Object.assign(order.customer,
      {'roomName': order.room.RoomName},
      this.order.getAppointmentFireStore());
    return this.dataService.saveOrderDetails(orderDetails);
  }

  Next() {
    this.addPanel();
    this.onAfterPanelAdd();
  }

  onAfterPanelAdd() {
    if (this.CurrentPanel === 4) {
      this.resetFinalStage();
      this.dataService.getTransDetails(this.order.transactionInfo.token).subscribe
      (s => this.handleTransDetailsResult(s), (e)=> {
        console.log("trans err");
        console.log(e);
        this.appointmentInvalid = true;
        this.sendMail('קרדיט גארד');
        }
      );
    }
  }

  resetFinalStage(){
    this.isTransDetailsReceived = false;
    this.appointmentInvalid = false;
    this.orderCompletedSuccessfully = false;
  }


  addPanel() {
    if (!(this.CurrentPanel + 1 > this.NumberOfPanels)) {

      this.Panels[this.CurrentPanel].Show = false;
      this.CurrentPanel += 1;
      this.Panels[this.CurrentPanel].Show = true;
    }
  }

  Back() {
    if (!(this.CurrentPanel - 1 < 0)) {
      this.Panels[this.CurrentPanel].Show = false;
      this.CurrentPanel--;
      this.Panels[this.CurrentPanel].Show = true;
    }
  }

  getLocalStorage = (key) => {
    var retrivedItem = JSON.parse(localStorage.getItem(key));
    if(!retrivedItem) return;
    if(new Date().getTime()  > retrivedItem.expirationTime){
      localStorage.removeItem(key);
    }
    else{
      return retrivedItem.value;
    }
  }

  setLocalStorage =(key, value, expirationMin)=>{
    localStorage[key] = JSON.stringify({value:value, expirationTime: (new Date().getTime() +
    expirationMin*60000)});
  }
}

