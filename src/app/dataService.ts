import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {Order} from "./models/order";
import { environment } from '../environments/environment';

export class DataService {
  http;
  afs;
  private isOldSafari: boolean|any;

  getCreditCardIframeUrlAndTranId(params) : Observable<string>{
    return (this.http.post(environment.firebase.apiUrl + '/getCGUrl', params).map
    ((res: Response) => {
       return res.text();
     }));
  }

  getRoomsList(roomId){
    const url = 'https://www.vcita.com/v/' + roomId + '/online_scheduling/business_structure';
    return {"categories":[{"uid":"3J6YYLVBN0RJ4QYC",
      "name":"\u05d4\u05e9\u05d9\u05e8\u05d5\u05ea\u05d9\u05dd \u05e9\u05dc\u05d9",
      "service_ids":["x5o91sj3ceb8bwq8","1vdehw1xtbmwqwlq"]}],
      "services":[{"id":2232447,"uid":"x5o91sj3ceb8bwq8","service_type":"appointment",
        "name":"\u05d4\u05d6\u05de\u05e0\u05ea \u05de\u05e9\u05d7\u05e7 - " +
        "\u05e9\u05d2\u05e8\u05d9\u05e8\u05d5\u05ea \u05d1\u05de\u05e6\u05d5\u05e8",
        "charge_type":"no_price","payment_text":"\u05dc\u05d0 \u05de\u05d5\u05e6\u05d2",
        "show_notes":false,"meeting_length_text":"\u05e9\u05e2\u05d4","type_class":"icon-twousers"
        ,"duration_text":"\u05e9\u05e2\u05d4","interval":0,"on_menu":true,"interaction_type":
          "pivot_location","interaction_type_text":"\u05e4\u05d2\u05d9\u05e9\u05d4","description":
          "","joint_availability":"no","staff_selection":"list","approval_mode":"immediate",
        "future_bookings_limit":0,"sms_reminders_enabled":true,
        "photo_path":"https://c15117557.ssl.cf2.rackcdn.com/avatar/image/378600/normal_g2cha65n1rlepglej8zfran9fsbydupn.jpg","available_staff":["zo1mf6fzh1gjusy5"]},{"id":2655449,"uid":"1vdehw1xtbmwqwlq","service_type":"appointment","name":"\u05d4\u05d6\u05de\u05e0\u05ea \u05de\u05e9\u05d7\u05e7 - \u05d0\u05d6\u05d5\u05e851","charge_type":"no_price","payment_text":"\u05dc\u05d0 \u05de\u05d5\u05e6\u05d2","show_notes":false,"meeting_length_text":"\u05e9\u05e2\u05d4","type_class":"icon-twousers","duration_text":"\u05e9\u05e2\u05d4","interval":0,"on_menu":true,"interaction_type":"pivot_location","interaction_type_text":"\u05e4\u05d2\u05d9\u05e9\u05d4","description":"","joint_availability":"no","staff_selection":"list","approval_mode":"immediate","future_bookings_limit":0,"sms_reminders_enabled":true,"photo_path":"https://c15117557.ssl.cf2.rackcdn.com/avatar/image/378605/normal_xyhdvn0zcbav80w081budcma2jfyim6u.jpg","available_staff":["ad58809fbe39a551"]}],"staff":[{"id":907998,"uid":"zo1mf6fzh1gjusy5","display_full_name":"\u05e9\u05d2\u05e8\u05d9\u05e8\u05d5\u05ea \u05d1\u05de\u05e6\u05d5\u05e8 - \u05d0\u05e4\u05e2\u05dc 24 \u05e4\u05f4\u05ea","title":null,"timezone":"Jerusalem","thumb_photo":"https://c15117557.ssl.cf2.rackcdn.com/avatar/image/376912/thumb_98j620dsnj913qzjemkrehpwrf5sp1w8.jpg","available_services":["x5o91sj3ceb8bwq8"]},{"id":1028212,"uid":"ad58809fbe39a551","display_full_name":"\u05d0\u05d6\u05d5\u05e851 - \u05d0\u05e4\u05e2\u05dc 24 \u05e4\u05f4\u05ea","title":null,"timezone":"Jerusalem","thumb_photo":"https://c15117557.ssl.cf2.rackcdn.com/avatar/image/376913/thumb_o6rvfwaxj8ojgikh0iai50k4f4rfv45x.jpg","available_services":["1vdehw1xtbmwqwlq"]}]}
  }

  getTransDetails (token): Observable<string>{
    console.log("get")
    return this.http.get(environment.firebase.apiUrl + "/getTransactionDetails?token=" + token)
      .map((res: Response) => (res.json()));
  }
  getLockedDates(){
    // if (!this.isOldSafari){
    // }
    // else{
    //   return this.http.get('https://us-central1-escaperooms-7dfd2.cloudfunctions.net/' +
    //     'getLockedDates/').map((res: Response) => { return (res.json())});
    // }
    var t = this.afs.collection("lockedDates", ref => ref.where('expirationDate', '>' ,new Date().getTime()));
    return t.valueChanges();
  }

  createNewClientAndAppointmentInVcita(clientAndAppointment){
    return this.http.post(environment.firebase.apiUrl + "/newClientAndAppointment",
      clientAndAppointment).map((res: Response) => res.text());
  }

  saveOrderDetails(orderDetails){
    //format date.
    
    return this.http.post(environment.firebase.apiUrl + "/orderDetails",
      orderDetails).map((res: Response) => res.text());
  }

  constructor(http, afs) {
    this.http = http;
    this.afs = afs;
    var navigator = window['navigator'];
    this.isOldSafari =(navigator['sayswho'].toLowerCase().indexOf("safari") != -1 &&
    navigator['sayswho'][navigator['sayswho'].length-1] < 10);
   }

  sendErrorToMail(mail) {
    if (!mail) return;
    return this.http.post(environment.firebase.apiUrl + "/sendEmail",
      mail).map((res: Response) => res.text());
  }
}
