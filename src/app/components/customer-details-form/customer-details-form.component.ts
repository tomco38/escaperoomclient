import {ApplicationRef, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DataService} from "../../dataService";
import {AngularFirestore} from "angularfire2/firestore";
import {Http} from "@angular/http";
import {Customer} from "../../models/customer";
import * as config from '../../config';
import {Modal} from 'ngx-modialog/plugins/bootstrap';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'customer-details-form',
  templateUrl: './customer-details-form.component.html',
  styleUrls: ['./customer-details-form.component.css']
})
export class CustomerDetailsFormComponent implements OnInit {
  @Input() maxPlayers: number;
  @Input() minPlayers: number;
  numberOfPlayers: number;
  policyCB: any = false;
  isFormInvalid = false;
  customer: Customer;
  lockingDate: boolean;
  @ViewChild('heroForm') form: NgForm;
  changeTimeMsg:boolean =false;
  dataService: DataService;
  @Output() onSubmit: EventEmitter<any> = new EventEmitter();
  @Output() back: EventEmitter<any> = new EventEmitter();

  constructor(private http: Http, public modal: Modal, private afs: AngularFirestore, private ref: ApplicationRef) {
    this.dataService = new DataService(this.http, this.afs);
    this.customer = new Customer();
  }

  ngOnInit() {
    this.numberOfPlayers = this.minPlayers;
    console.log("numOfPlayers" + this.numberOfPlayers);
  }

  AddPlayer() {
    if (this.numberOfPlayers !== this.maxPlayers) {
        this.numberOfPlayers++;
    }
  }

  RemovePlayer() {
    if (this.numberOfPlayers > this.minPlayers) {
      this.numberOfPlayers--;
    }
  }

  private isUserFormInvalid(): boolean {
    let inputs: NodeListOf<any> = document.getElementsByTagName("input");
    Object.keys(this.form.controls).forEach(key => {
      this.form.controls[key].markAsDirty();
      this.form.controls[key].markAsTouched();
    });
    if (!this.policyCB) return true;
    for (let i = 0; i < inputs.length; i++) {
      if (inputs.item(i).classList.contains("ng-invalid")) {
        return true;
      }
    }
    return false;
  }
  openModal() {
    this.modal.alert()
      .size('lg')
      .showClose(true)
      .title(config.title)
      .body(config.modalBody).okBtn('סגור').okBtnClass("btn btn-primary ok-btn").dialogClass('myDialog').bodyClass('myDialogBody')
      .open();
  }

  next(){
    this.isFormInvalid = this.isUserFormInvalid();
    if (!this.isFormInvalid) {
      this.lockingDate = true;
      this.onSubmit.emit({customer: this.customer, numberOfPlayers:this.numberOfPlayers});
    }
    else return;
  }

  restartForm(){
    this.lockingDate = false;
    this.changeTimeMsg = false;
  }
  onAfterOrderSaved(){
    this.restartForm();
  }
  onAfterDateSelect(){
    this.restartForm();
  }
  dateIsLocked(){
    this.lockingDate = false;
    this.changeTimeMsg = true;
  }
}
