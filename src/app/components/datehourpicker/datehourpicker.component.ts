import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {SubRoom} from "../../models/subRoom";
import {Http, Response} from "@angular/http";
import {DataService} from "../../dataService";
import {AngularFirestore} from "angularfire2/firestore";
import {AvailableDates} from "../../bo/availableDates";
import {AvailablesHours} from "../../bo/avialableHours";

@Component({
  selector: 'datehourpicker',
  templateUrl: './datehourpicker.component.html',
  styleUrls: ['./datehourpicker.component.css']
})
export class DatehourpickerComponent implements OnInit {

  Days: Array<string> = [
    'יום ראשון',
    'יום שני',
    'יום שלישי',
    'יום רביעי',
    'יום חמישי',
    'יום שישי',
    'יום שבת'
  ];
  Months: Array<string> = [
    'ינואר',
    'פברואר',
    'מרץ',
    'אפריל',
    'מאי',
    'יוני',
    'יולי',
    'אוגוסט',
    'ספטמבר',
    'אוקטובר',
    'נובמבר',
    'דצמבר'
  ];

  DaysArray: AvailableDates[] = [];
  Hours_av: AvailablesHours[] = [];
  datePickerMonth:any = {};
  datesGroupedByMonths = [];
  Pick_A_Date: AvailableDates;
  Pick_A_Hour: AvailablesHours;
  CurrentMonth: number;
  showError : boolean;
  errorText : string;

  midnightMsg;

  dataService: DataService;
  @Input() subRoom: SubRoom;
  ready: boolean = false;


  @Output() back: EventEmitter<any> = new EventEmitter();
  @Output() dateChoose: EventEmitter<any> = new EventEmitter();

  constructor(private http: Http, private afs: AngularFirestore) {
    this.dataService = new DataService(this.http, this.afs);
    this.Pick_A_Date = new AvailableDates();
    this.Pick_A_Hour = new AvailablesHours();
    this.CurrentMonth = 0;
  }

  ngOnInit() {
    this.DaysArray = this.getDaysArray(70);
    this.DaysArray.pop();
    this.createDatePicker(this.subRoom.StaffId,
      this.subRoom.ServiceUID, this.DaysArray[0]._Date,
      this.DaysArray[this.DaysArray.length - 1]._Date);
  }

  /* Validating date and hour */
  next(){

    if(!(this.Pick_A_Date._WeekDay && this.Pick_A_Date._Date)){
      this.errorText = "תאריך ושעה";
      this.showError = true;
      return;
    }
    if(!( this.Pick_A_Hour.Time && this.Pick_A_Hour.IsChoosen)){
      this.errorText = "שעה";
      this.showError = true;
      return;
    }

    this.showError = false;
    this.dateChoose.emit({date:this.Pick_A_Date, hour: this.Pick_A_Hour});
  }

  getRoomAvailability(staff_id: string, service_uid: string, start: string, end: string){
    const url = 'https://www.vcita.com/pivot/availability/' + staff_id +
      '?time_zone=Jerusalem&service_uid=' + service_uid +
      '&service_type=appointment&staff_id=' + staff_id + '&start=' + start + '&end=' + end;
    return this.http.get(url).map((res: Response) => res.json());
  }

  createDatePicker(staff_id: string, service_uid: string, start: string, end: string) {
    this.getRoomAvailability(staff_id, service_uid, start, end).subscribe(availableDates => {
      console.log("getLock");
      // this.dataService.getLockedDates().subscribe((lockedDates) => {
      //   console.log("res");
        this.fillDatePicker(availableDates, "");
      // });
    });
  }

  GetCalander(val, lockedDates) {
    for (var i = this.DaysArray.length - 1; i >= 0; i--) {
      var date = this.DaysArray[i];
      const av_time = val.daily_availability[date._Date];
      if ( av_time) {
        var timeArray = [];
        for (const time of av_time) {
          var addTime = true;
          var timeToAdd = this.converTimeStampIntoDate(time);
          if (lockedDates && lockedDates != "") {
            var timeStamp = new Date(date._Date + ' ' + timeToAdd).getTime();
            if ((lockedDates.some((x) => x.lockedDate == timeStamp))) addTime = false;
          }
          if (addTime)
            date.AddAv_time(timeToAdd, timeArray);
        }
        date._AvTime = timeArray;
      }
      else{
        this.DaysArray.splice(i,1);
      }
    }
  }

  converTimeStampIntoDate(value): string {

    const date = new Date(value * 1000);
    const hours = date.getUTCHours();
    const minutes = '0' + date.getUTCMinutes();

    return hours + ':' + minutes.substr(-2);
  }

  getDaysArray(amountOfDays: number) {

    const Today = new Date();
    let Year, Month, Day;


    Year = Today.getFullYear().toString();
    Month = (Today.getMonth() + 1).toString();

    if (Month.length === 1) {

      Month = '0' + Month;
    }

    Day = Today.getDate().toString();

    if (Day.length === 1) {

      Day = '0' + Day;
    }

    this.DaysArray.push(new AvailableDates(Year + '-' + Month + '-' + Day, this.Days[Today.getDay()]));

    for (let i = 1; i < amountOfDays; i++) {

      Today.setDate(Today.getDate() + 1);

      Year = Today.getFullYear().toString();
      Month = (Today.getMonth() + 1).toString();

      if (Month.length === 1) {

        Month = '0' + Month;
      }

      Day = Today.getDate().toString();

      if (Day.length === 1) {

        Day = '0' + Day;
      }

      this.DaysArray.push(new AvailableDates(Year + '-' + Month + '-' + Day, this.Days[Today.getDay()]));
    }
    return this.DaysArray;
  }

  maxDatepickerMonths = 10;

  changeDatepickerMonth(monthsToAdd){
    var newIndex = this.datePickerMonth.index + monthsToAdd;
    if (newIndex < 0 || newIndex > this.datesGroupedByMonths.length - 1) return;
    this.datePickerMonth = this.setDatepickerMonth(this.addMonths(this.datePickerMonth.date,monthsToAdd),
      newIndex);

    // Select the first date for each month
    if(this.datesGroupedByMonths[this.datePickerMonth.index].data.length > 0)
      this.ChooseDate(this.datesGroupedByMonths[this.datePickerMonth.index].data[0]);
  }

  setDatepickerMonth(date, index){
    var datePickerMonth:any = {};
    datePickerMonth.date = date;
    datePickerMonth.index = index;
    datePickerMonth.text = this.Months[date.getMonth()] + ' ' + date.getFullYear();
    return datePickerMonth;
  }

  addMonths(date, months) {
    var newDate = new Date(date);
    var d = newDate.getDate();
    newDate.setMonth(newDate.getMonth() + months);
    if (newDate.getDate() != d) {
      newDate.setDate(0);
    }
    return newDate;
  }

  ChooseDate(val: AvailableDates) {

    this.Pick_A_Date._IsChoosen = false;

    this.Pick_A_Hour.IsChoosen = false;
    this.Pick_A_Date = val;

    this.Pick_A_Date._IsChoosen = true;

    this.Hours_av = val._AvTime;
  }
  changeTimeMsg = false;

  timeIsMidnight(time){return time.startsWith("0:")}
  getNextDayName(weekDay){return this.Days[(this.Days.indexOf(weekDay) + 1) % 7];
  }
  ChooseTime(val: AvailablesHours) {
    this.Pick_A_Hour.IsChoosen = false;
    this.Pick_A_Hour = val;
    this.Pick_A_Hour.IsChoosen = true;

    val.IsChoosen = true;
    this.changeTimeMsg = false;

    if(this.timeIsMidnight(val.Time)){
      var nextDay = this.getNextDayName(this.Pick_A_Date._WeekDay);
      this.midnightMsg =this.Pick_A_Date._WeekDay + " ל" + nextDay;
    }
    else{
      this.midnightMsg = "";
    }
  }

  groupDatesByMonth(){
    var groups = this.DaysArray.reduce(function (r, o) {
      var m = o._Date.split(('-'))[1];
      (r[m])? r[m].data.push(o) : r[m] = {month: (m), data: [o]};
      return r;
    }, {});
    return Object.keys(groups).map(function(k){
      return groups[k];
    });
  }

  MonthForward() {
    if (this.CurrentMonth === 11) {
      this.CurrentMonth = 0;
    } else this.CurrentMonth++;

  }

  MonthBackward() {
    if (this.CurrentMonth === 0) {
      this.CurrentMonth = 11;
    } else this.CurrentMonth--;

  }

  private fillDatePicker(availableDates, lockedDates) {
    // var lockedDates = this.filterLocalStorageLockedDates(lockedDates);
    this.GetCalander(availableDates, lockedDates);
    // this.DaysArray.pop();
    this.datesGroupedByMonths = this.groupDatesByMonth();
    this.datePickerMonth.date = new Date(this.datesGroupedByMonths[0].data[0]._Date);
    this.datePickerMonth.index = 0;
    this.changeDatepickerMonth(0);
    this.ChooseDate(this.DaysArray[0]);
    this.ready = true;
  }

  clearDateSelection(){
    this.Pick_A_Date._IsChoosen = false;
    this.Pick_A_Hour.IsChoosen = false;
    this.Pick_A_Date._IsChoosen = false;
    this.Hours_av = [];
  }
}
