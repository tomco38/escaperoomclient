import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {DataService} from "../../dataService";
import {Http} from "@angular/http";
import {AngularFirestore} from "angularfire2/firestore";
import {Order} from "../../models/order";

@Component({
  selector: 'cg-iframe',
  templateUrl: './cg-iframe.component.html',
  styleUrls: ['./cg-iframe.component.css']
})
export class CgIframeComponent implements OnInit {

  dataService: DataService;
  creditCardIframeUrl: string;
  @Input() email: string;
  @Input() fullName: string;
  @Output() onCreditGuardCallback: EventEmitter<any> = new EventEmitter();
  @Output() paymentSuccess: EventEmitter<any> = new EventEmitter();
  @Output() back: EventEmitter<any> = new EventEmitter();
  @Output() error: EventEmitter<any> = new EventEmitter();
  @Input() order: Order;

  constructor(private http: Http, private afs: AngularFirestore) {
    this.dataService = new DataService(this.http, this.afs);
    console.log(this.order);
  }

  ngOnInit() {
    this.subscribeToCreditGuardIframeMsgs();
    console.log(this.order);
    console.log(this.order.roomDetails.RoomName);
    this.getCreditCardIframeUrl(this.email, this.fullName, this.order.roomDetails.RoomName);
    // TODO: subscribe to orderCompletedSuccessfully :)
  }

  subscribeToCreditGuardIframeMsgs() {
    var cThis = this;
    window.addEventListener("message", function (event) {
        cThis.paymentSuccess.emit();
    });
  }

  private getCreditCardIframeUrl(email, fullName, roomName) {
    this.dataService.getCreditCardIframeUrlAndTranId({
      email: email, fullName: fullName, roomName: roomName
    }).subscribe(u => this.creditCardUrlCallBack(u),
      (err)=>{this.error.emit();});
  }

  private creditCardUrlCallBack(res) {
    var resJson = JSON.parse(res);
    var url = resJson.url;
    this.creditCardIframeUrl = url;
    this.onCreditGuardCallback.emit({token: url.substr(url.indexOf('=') + 1),
                                             transactionId: resJson.tranId});
  }

}
