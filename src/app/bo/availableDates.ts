import {AvailablesHours} from "./avialableHours";
export class AvailableDates {

  _Date: string;
  _WeekDay: string;
  _AvTime: AvailablesHours [] = [];
  _IsChoosen: boolean = false;
  public TimeRev: string;
  constructor(date?: string, day?: string) {
    console.log("agag");
    this._Date = date || '';
    this._WeekDay = day || '';
  }

  // AddAv_time(val) {
  //
  //   this._AvTime.push(new AvailablesHours(val));
  //   this.ReverseTime();
  // }
  AddAv_time(val, arr) {

    if(val.length == 4 ) // fixing format m:ss to mm:ss
      val = "0" + val;

    arr.push(new AvailablesHours(val));
    this.ReverseTime();
    return arr;
  }

  ReverseTime() {

    const splitted = this._Date.split('-', 3);

    this.TimeRev = splitted[2] + '-' + splitted[1] + '-' + splitted[0];

  }
  
  AddAv_timeLateNight(val, lastArr){

    if(val.length == 4 ) // fixing format m:ss to mm:ss
      val = "0" + val;

     lastArr.push(new AvailablesHours(val));
     this.ReverseTime();
     return lastArr;
  }

  getHours(){
    console.log(this._AvTime);
    return this._AvTime;
  }
}
