﻿export class PanlesManager {


    Panels: Panel[] = [];
    NumberOfPanels: number;
    CurrentPanel: number;


    getCurrentPanel(){
      return this.CurrentPanel;
    }
    /*NOT GENERIC*/
    RenderPage_1: boolean = false;
    RenderPage_1_Violation = false;

    constructor(NumberOfPanels: number) {
      this.setNumberOfPanels(NumberOfPanels);
    }

    setNumberOfPanels(NumberOfPanels: number){
      this.NumberOfPanels = NumberOfPanels;
      this.CurrentPanel = 0;

      for (let panelNumber = 0; panelNumber < this.NumberOfPanels; panelNumber++) {

          this.Panels.push(new Panel(panelNumber));
      }

      this.Panels[0].Show = true;
    }


    // RandomAccessChangePage(pageTo: number) {
    //
    //     if (this.CurrentPanel === 0) {
    //
    //         if (this.RenderPage_1 === false) {
    //             this.RenderPage_1_Violation = true;
    //             return;
    //         }
    //     }
    //
    //     if (pageTo !== this.CurrentPanel) {
    //
    //         this.Panels[this.CurrentPanel].Show = false;
    //         this.CurrentPanel = pageTo;
    //         this.Panels[this.CurrentPanel].Show = true;
    //     }
    // }

    ChangeStateEnter(num: number) {

        this.Panels[num].State = false;
    }

    ChangeStateLeave(num: number) {

        this.Panels[num].State = true;
    }
}




export class Panel {

    Show: boolean;
    State: boolean;
    Number: number;

    constructor(panelNumber: number) {
        this.Number = panelNumber;
        this.Show = false;
        this.State = true;
    }
}
