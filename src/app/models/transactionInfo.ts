export class TransactionInfo {
  token: string;
  transactionId: number;
  amount: number;
  description: string;

  constructor(){}
}
