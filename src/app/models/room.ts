import { SubRoom } from './subRoom';

export class Room {

  RoomId: string;
  _Res: any
  uid: string;

  stafId: string[] = [];

  BoolOpengate: false;

  SubRoomList: SubRoom[] = [];

  constructor(id: string, Res: any) {

    this.RoomId = id;
    this._Res = Res;
  }

  ParseResponse(): boolean {

    this.uid = this._Res.categories[0].uid;

    for (const staf of this._Res.staff) {
      this.stafId.push(staf.uid);
    }

    for (const service of this._Res.services) {

      this.SubRoomList.push(new SubRoom(
        service.name,
        service.uid,
        service.photo_path,
        this.stafId.shift(),
        service.uid
      ));
    }
    return true;
  }
}
