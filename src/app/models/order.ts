import { SubRoom } from "./subRoom";
import { Customer } from "./customer";
import { TransactionInfo } from "./transactionInfo";
import {AvailablesHours} from "../bo/avialableHours";
import {AvailableDates} from "../bo/availableDates";

export class Order {
  //subRoom: Room;
  id: number;
  room: SubRoom;
  roomDetails;
  numberOfPlayers: number;
  customer:Customer;
  transactionInfo: TransactionInfo;
  date: AvailableDates;
  hour: AvailablesHours;
  constructor(){
    this.customer = new Customer();
    this.transactionInfo = new TransactionInfo();
    this.numberOfPlayers = 3;
  }

  getAppointmentVcita():any{
    return {
      interaction_type: 'business_location',
      start_time: this.date._Date + ' ' + this.hour.Time + ':00 UTC',
      duration: '60',
      title:   this.customer.firstName + ' ביצע ' + this.roomDetails.RoomName,
      roomName: this.roomDetails.RoomName,
      notes: 'הוזמן על ידי slot secure, מספר משתתפים ' + this.numberOfPlayers,
      service_id: this.roomDetails.ServiceId,
      staff_id: this.roomDetails.StaffId,
      interaction_details :  'אפעל 24 פ״ת',
      tranId: this.transactionInfo.transactionId
    }
  }
  getAppointmentFireStore():any{

    var tempDate = this.date._Date.replace(/-/g,"/");
    return {
      service_id: this.roomDetails.ServiceId,
      staff_id: this.roomDetails.StaffId,
      lockTime: new Date(tempDate + ' ' + this.hour.Time).getTime(),
      time: this.date._Date + ' ' + this.hour.Time + ':00 UTC',
      numberOfPlayers :  this.numberOfPlayers,
    }
  }
  getMail(error){
    return{
      mail:  "שגיאה ב " + error + "\n" +  "פרטים אישים: \n" +
      "שם: " +  this.customer.getFullName() +", \n" +
      "מייל: " + this.customer.email + ", \n" +
      "טלפון: " + this.customer.telephone + ", \n" +
      "מס טרנזקציה: " + this.transactionInfo.transactionId + ", \n" +
      "חדר: " + this.room.RoomName+ ", \n" +
      "מס משתתפים: " + this.numberOfPlayers + ", \n" +
      "זמן: "+ this.date._Date + ' ' + this.hour.Time
    }
  }

}
