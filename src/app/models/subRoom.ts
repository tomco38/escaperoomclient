export class SubRoom {

  RoomName: string;
  ServiceUID: string;
  ServiceId;
  SubRoomPhoto: string;
  IsChoosen: boolean;
  StaffId: string;

  constructor(roomName: string, subId: string, subRoomPhoto: string, staffId: string, serviceId) {

    this.ServiceId = serviceId;
    this.RoomName = roomName;
    this.ServiceUID = subId;
    this.SubRoomPhoto = subRoomPhoto;
    this.StaffId = staffId;
    this.IsChoosen = false;
  }


  ChooseRoom() {
    this.IsChoosen = true;
  }

}
