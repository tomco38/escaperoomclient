  export class Customer {
  id:string;
  firstName:string;
  lastName:string;
  telephone:number;
  email:string;

  constructor(){}

  getVcitaClientJsonFormat():any{
    return {
      first_name:this.firstName,
      last_name:this.lastName,
      email:this.email,
      phone:this.telephone
    }
  }
  getFullName():string{
    return this.firstName + ' ' + this.lastName;
  }
}
